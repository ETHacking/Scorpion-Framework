﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace RawSock
{
    class HttpFile
    {
        public int respCode;

        public string url, contentType, headers, response;

        public List<string> links;

        public HttpFile(string link)
        {
            url = link;

            int ioxCount = 0;
            string getReq = "HEAD " + link + " HTTP/1.1\r\n" +
            "Host: " + HTTPhandler.dom +
            "\r\n\r\n";

            byte[] msg = Encoding.ASCII.GetBytes(getReq);

            byte[] header = new byte[1000];

            HTTPhandler.writeStream(msg);

            while (true)
            {
                try
                {
                    HTTPhandler.readStream(ref header);

                    headers = Encoding.ASCII.GetString(header);

                    respCode = getFileCode();
                    contentType = getFileType();

                    break;
                }
                catch (System.IO.IOException)
                {
                    HTTPhandler.changeTimeout(5000);
                    ioxCount++;

                    if (ioxCount == 2)
                        break;
                }
                catch (System.FormatException)
                {
                    //apparently first response we get in a set of random bytes, so we dirtly catch it here
                }
            }

        }

        private int getFileCode()
        {
            //HTTP[^ ]+ (\d+) ([^\n]+)
            Regex regex = new Regex("HTTP[^ ]+ (\\d+) ([^\n]+)");

            return int.Parse(regex.Match(headers).Groups[1].ToString());

        }

        private string getFileType()
        {
            //[Cc]ontent-[Tt]ype: (.+\/.+)\s
            Regex regex = new Regex("[Cc]ontent-[Tt]ype: (.+\\/.+)\\s");

            string cType = regex.Match(headers).Groups[1].ToString();

            if (cType.Contains(';'))
            {
                cType = cType.Substring(0, cType.IndexOf(';'));
            }

            return cType;
        }

        //takes up a lot of time to do, so only some of the files will be set up
        public void setUp()
        {
            Console.WriteLine("-------- Receiving file " + url + " --------");

            string getReq = "GET " + url + " HTTP/1.1\r\n" +
            "Host: " + HTTPhandler.dom +
            "\r\n\r\n";

            byte[] msg = Encoding.ASCII.GetBytes(getReq);

            HTTPhandler.writeStream(msg);

            response = HTTPhandler.readRecursive();

            Console.WriteLine("-------- File " + url + "  received --------");

            Console.WriteLine();

            Console.WriteLine(">> Getting links in file...");
            links = getFileLinks();
            Console.WriteLine("Done!");

            Console.WriteLine();
            Console.WriteLine();
        }

        private List<string> getFileLinks()
        {

            //(https?)?[^\w<](\/[\w?\/](?:[^ "'\^]+)?)(\n|"|'|\);)
            Regex regexLink = new Regex("(https?)?[^\\w<](\\/[\\w?\\/](?:[^ \"'\\^]+)?)(\n|\"|'|\\);)");
            //[] para buscar caracteres específicos, ^ para excluirlos
            //{} indica repeticion de caracteres combinando con secuencias en [] se pueden hacer cosas chascas
            //+ al menos uno de el caracter, * cualquier caracter igual (se pueden mandar a tomar por saco si usas {x,y})
            //? para indicar la opcionalidad del anterior caracter
            // \s cualquier tipo de espacio: \r return \n linea nueva \t tab ␣ espacio
            // | or (puerta logica)
            // \w alfanumerico \d numerico INVERSOS: \W \D
            // \b para cuando se topa con un caracter no alphabetico (\w+\b)
            List<string> linksEX = new List<string>();
            List<string> linksLAN = new List<string>();
            foreach (Match match in regexLink.Matches(response))
            {
                if (!linksEX.Contains(match.ToString()))
                {

                    //check if the protocol group is filled
                    if (match.Groups[1].ToString() != "")
                    {
                        if (match.Groups[2].ToString()[1] == '/')
                        {
                            if (match.Groups[2].ToString().Substring(2).Split('/')[0].Contains(HTTPhandler.dom))
                            {
                                //this link appears to be local, since the domain of the target host is in the url

                                if (match.Groups[2].ToString().Substring(2).Split('/').Length > 1)  //else this means it's just a reference to the host
                                    linksLAN.Add('/' + match.Groups[2].ToString().Substring(2).Split('/')[1]);
                            }
                            else
                            {
                                linksEX.Add(match.Groups[1].ToString() + ':' + match.Groups[2].ToString());
                            }
                        }
                        else
                            linksEX.Add(match.Groups[1].ToString() + ":/" + match.Groups[2].ToString());
                    }
                    else
                    {
                        //if double slash, add safe protocol and external link
                        if (match.Groups[2].ToString()[1] == '/')
                            linksEX.Add("https:" + match.Groups[2].ToString());
                        else //else add host link
                            linksLAN.Add(match.Groups[2].ToString());
                    }

                }
            }

            return linksLAN;
        }
    }
}
