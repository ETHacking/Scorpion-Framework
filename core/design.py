#/usr/bin/env python

import random


class color:
    RED = '\033[0;91m'
    GREEN = '\033[0;92m'
    YELLOW = '\033[0;93m'
    BLUE = '\033[0;94m'
    PURPLE = '\033[0;95m'
    NC = '\033[0m'
    ERROR = RED + '[' + '!' + ']  '
    NOTIFICATION = GREEN + '['+ BLUE +'i'+ GREEN +']'+ BLUE + '  '
    WARNING = RED + '['+ YELLOW + '**' + RED +']'+ YELLOW + '  '
    OK = GREEN + '[' + '+' + ']' + '  '
    KERNELSUCCESS = GREEN + '[' + 'OK' + ']' + '  '
    KERNELNOTIFICATION = GREEN + '[' + BLUE + 'notification'+ GREEN + ']' + BLUE + '  '
    KERNELWARNING = RED + '[' + YELLOW + 'warning' + RED + ']' + YELLOW + '  '
    KERNELERROR = RED + '[' + 'failed' + ']'+'  '

color = color()

def main_cli(module): return color.BLUE + '[' + module + ']$ ' + color.GREEN


def banner():
    f = open('data/banner.txt', 'r')
    file_contents = f.read()
    print (color.RED + file_contents + color.NC)
    f.close()
