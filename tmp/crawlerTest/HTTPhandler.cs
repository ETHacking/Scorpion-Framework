﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Net.Sockets;
using System.Net.Security;
using System.Diagnostics;
using System.Threading.Tasks;

namespace RawSock
{
    abstract class HTTPhandler
    {
        static Stream stream;
        static TcpClient sock;
        public static string dom;
        public static bool safe = false;

        public static TcpClient connectSocket(string domain)
        {
            int port = 80;
            dom = domain;

            if (safe)
                port = 443;

            sock = new TcpClient();

            sock.Connect(dom, port);

            if (safe)
            {
                stream = new SslStream(sock.GetStream());
                ((SslStream)stream).AuthenticateAsClient(dom);
            }
            else
            {
                stream = sock.GetStream();
            }
            stream.ReadTimeout = 1000;

            return sock;
        }

        public static string readRecursive()
        {
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            string compResponse = "";
            
            Console.WriteLine("Starting read recursive");

            while (true)
            {
                try
                {
                    byte[] respuesta = new byte[20000];

                    int k = readStream(ref respuesta);

                    ClearCurrentConsoleLine();

                    Console.WriteLine("Getting file with length - " + compResponse.Length);
                    compResponse += Encoding.ASCII.GetString(respuesta, 0, k);

                    if (compResponse.Length > 50 * Math.Pow(10, 6))
                    {
                        Console.WriteLine("This file is over 50 MB, we'll cut it here");
                        break;
                    }

                    if (k == 0)
                    {
                        if (compResponse.Length == 0)
                            Console.WriteLine("Probably a wrong host (packet received with length of 0)");
                        break;
                    }

                }
                catch (System.IO.IOException)
                {
                    break;
                }
            }

            return compResponse;
        }

        public static int readStream(ref byte[] respuesta)
        {
            int length;

            if (safe)
                length = ((SslStream)stream).Read(respuesta, 0, respuesta.Length);
            else
                length = stream.Read(respuesta, 0, respuesta.Length);

            return length;
        }
        public static void writeStream(byte[] msg)
        {
            if (safe)
                ((SslStream)stream).Write(msg, 0, msg.Length);
            else
                stream.Write(msg, 0, msg.Length);
        }

        public static void closeSocket()
        {
            sock.Close();
        }

        public static void changeTimeout(int to)
        {
            stream.ReadTimeout = to;
        }

        public static void ClearCurrentConsoleLine()
        {
            Console.SetCursorPosition(0, Console.CursorTop - 1);
            int currentLineCursor = Console.CursorTop;
            Console.SetCursorPosition(0, Console.CursorTop);
            Console.Write(new string(' ', Console.WindowWidth));
            Console.SetCursorPosition(0, currentLineCursor);
        }
    }
}
