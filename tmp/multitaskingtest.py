import threading
import time

exitFlag = 0

# Define a function for the thread
class My_Thread(threading.Thread):
    def __init__(self, threadID, name, counter):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name
        self.counter = counter
    def run(self):
        print("Starting {0}...".format(self.name))
        print_time(self.name, 5, self.counter)
        print("Exiting from {0}...".format(self.name))

def print_time(threadName, counter, delay):
    while counter:
        if exitFlag:
            threadName.exit()
        time.sleep(delay)
        print("{0}: {1}".format(threadName, time.ctime(time.time())))
        counter -= 1

thread1 = My_Thread(1, "Thread-1", 1)
thread2 = My_Thread(2, "Thread-2", 2)

thread1.start()
thread2.start()

print("Exiting the Main thread")
