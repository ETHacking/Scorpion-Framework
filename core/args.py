import sys

def DebugMode(argv):
    for arg in argv:
        if arg == "-d":
            return 1
    return 0
