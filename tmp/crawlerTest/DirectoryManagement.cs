﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RawSock
{
    abstract class DirectoryManagement
    {
        public static void createDirs(string path)
        {
            try
            {
                Directory.CreateDirectory(path);
            }
            catch (System.ArgumentException ae)
            {
                Console.WriteLine(path + " was probably not a file :(");
            }
        }

        public static void createDirsAndFile(string dom, string path, string fileName)
        {
            try
            {
                Directory.CreateDirectory(dom + path);
                if (!File.Exists(dom + path + fileName))
                    File.Create(dom + path + fileName);
            }
            catch (System.ArgumentException ae)
            {
                Console.WriteLine(path + fileName + " was probably not a file :(");
            }
        }
    }
}
