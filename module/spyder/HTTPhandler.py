import sys
import socket
import ssl

class HTTPhandling:

    stream = None
    sock = None
    dom = ""
    safe = False

    def connectSocket(self, domain):
        self.port = 80
        self.dom = domain

        if self.safe:
            self.port = 443

        self.sock =  socket.socket()

        if self.safe:
            context = ssl.create_default_context(ssl.Purpose.SERVER_AUTH)
            context.options |= ssl.OP_NO_TLSv1 | ssl.OP_NO_TLSv1_1  # optional
            self.stream = context.wrap_socket(self.sock, server_hostname=domain)
        else:
            self.stream = self.sock
        
        self.stream.connect((self.dom, self.port))

        self.stream.settimeout(1)

        return self.stream

    def readRecursive(self):
        compResponse = ""
        
        print("Starting read recursive")

        while True:
            try:
                respuesta = self.readStream()

                print("Getting file with length - " + str(len(compResponse)), end='\r')
                compResponse += respuesta

                if len(compResponse) > 50 * 10**6:
                    print("This file is over 50 MB, we'll cut it here")
                    break

                if len(respuesta) == 0:
                    if len(compResponse) == 0:
                        print("Probably a wrong host (packet received with length of 0)")
                    break

            except IOError:
                break

        return compResponse

    def readStream(self):
        respuesta = self.stream.recv(20000)

        return respuesta.decode('ISO-8859-1')

    def writeStream(self, msg):
        self.stream.send(str.encode(msg))

    def closeSocket(self):
        self.stream.close()

    def changeTimeout(self, to):
        self.stream.settimeout(to)

