﻿using System;
using System.Text.RegularExpressions;
using System.Text;
using System.Collections.Generic;
using System.Threading;

namespace RawSock
{
    class Module
    {
        static string dom = "";
        static int browsingDepth = 1;

        static List<string> analizedLinks = new List<string>();

        static string getDomain(string url, ref string file)
        {
            string dom = url;

            if (url.Contains("://"))
            {
                if (url.Substring(0, url.IndexOf("://")).Equals("https"))
                    HTTPhandler.safe = true;
                dom = url.Substring(url.IndexOf("://") + 3);
            }

            if (dom.Split(new char[] { '/' }, 2).Length > 1)
            {
                file += dom.Split('/')[1];
            }

            dom = dom.Split('/')[0];

            return dom;
        }

        public static int Main(string[] arg)
        {
            string fileUrl = "/";

            browsingDepth = int.Parse(arg[0]);

            dom = getDomain(arg[1], ref fileUrl);

            Console.WriteLine("Connecting.....");

            HTTPhandler.connectSocket(dom);

            Console.WriteLine("Connected");

            Console.WriteLine();
            Console.WriteLine();

            HttpFile index = new HttpFile(fileUrl);

            if (index.respCode != 200)
            {
                Console.WriteLine("Unexpected response code: " + index.respCode);
                return 1;
            }

            analizeFile(index, 0);

            Console.WriteLine("end");


            return 0;
        }

        static void analizeFile(HttpFile file, int depth)
        {
            file.setUp();

            Console.WriteLine();
            Console.WriteLine("-------------------------------------------------------------------------");
            Console.WriteLine(">> Analizing file " + file.url + " in depth of " + depth + "...");
            Console.WriteLine("\\____/-------------------------------------------------------------\\____/");
            Console.WriteLine();

            foreach (string link in file.links)
            {

                HttpFile linkFile = new HttpFile(link);

                //basic sanitizing of the links

                if (basicUrlSanitize(ref linkFile.url))
                {
                    //here we validate the existance of the file in the host machine

                    Console.WriteLine(linkFile.url + "  " + linkFile.respCode + " " + linkFile.contentType);

                    if (linkFile.respCode != 404 && linkFile.respCode != -1)
                    {
                        //if a link ends up in a slash, we can assume it's a folder reference

                        if (linkFile.url[linkFile.url.Length - 1] == '/')
                        {
                            if (linkFile.url.Contains("?"))
                                //this type of url shouldn't contain any arguments
                                Console.WriteLine(linkFile.url + " was probably not a file :(");
                            else
                                DirectoryManagement.createDirs(dom + linkFile.url);
                        }
                        else
                        {
                            //linkFile.url's value is something like /folder/file.extension or /file.extension at this point
                            string path = linkFile.url.Substring(0, linkFile.url.LastIndexOf('/'));
                            string fileName = linkFile.url.Substring(linkFile.url.LastIndexOf('/'));

                            if (sanitizePathToFile(path, ref fileName))
                            {
                                //if our pressumed file contains a . in it's name, then we can say it's an extension, probably
                                if (fileName.Contains("."))
                                    DirectoryManagement.createDirsAndFile(dom, path, fileName);
                                else
                                    DirectoryManagement.createDirs(dom + path + fileName);
                            }

                        }
                        if (linkFile.respCode == 200 && linkFile.contentType.Contains("text/html"))
                        {
                            //this means we have an index file in here
                            if (depth < browsingDepth && !analizedLinks.Contains(linkFile.url))
                            {
                                analizedLinks.Add(linkFile.url);
                                analizeFile(linkFile, depth + 1);
                            }
                        }
                    }
                }
                else
                {
                    Console.WriteLine(dom + linkFile.url + " was probably not a file :(");
                }
            }

            Console.WriteLine(" ____                                                              ____");
            Console.WriteLine("/    \\------------------------------------------------------------/    \\");
            Console.WriteLine(">> Analisis of file " + file.url + " in depth of " + depth + " concluded");
            Console.WriteLine("-------------------------------------------------------------------------");
            Console.WriteLine();
        }

        static bool sanitizePathToFile(string path, ref string fileName)
        {
            //here we get rid of all possible arguments given in the url
            if (fileName.Contains("?"))
                fileName = fileName.Substring(0, fileName.LastIndexOf('?'));

            //this wouldnt be possible
            if (path.Contains("?"))
                Console.WriteLine(dom + path + fileName + " was probably not a file :(");
            return !path.Contains("?");
        }

        static bool basicUrlSanitize(ref string link)
        {
            if (link.Contains("\\"))
            {
                link = link.Replace("\\", string.Empty);
            }

            char[] badChars = new char[] { '\"', '<', '>', '|' };

            return !(link.IndexOfAny(badChars) != -1);
        }

        

    }

}