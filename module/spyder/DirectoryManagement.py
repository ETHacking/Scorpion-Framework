import sys
import os

def createDirs(path):
    path = os.path.dirname(os.path.abspath(__file__)) + "/" + path
    try:
        os.makedirs(path)
    except OSError:
        print(path + " was probably not a file :(")

def createDirsAndFile(dom, path, fileName):
    dom = os.path.dirname(os.path.abspath(__file__)) + "/" + dom
    try:
        if not os.path.exists(dom + path):
            os.makedirs(dom + path)
        if not os.path.isfile(dom + path + fileName):
            f = open(dom + path + fileName, "w+")
            f.close()
    except OSError:
        print(dom + path + fileName + " was probably not a file :(")

def browse(dom, analized):
    dom = os.path.dirname(os.path.abspath(__file__)) + "/" + dom
    if os.path.exists(dom):
        if not analized:
            res = input("Browse previous results? Y/n (ls/cd/quit)").lower()
        else:
            res = input("Browse results? Y/n (ls/cd/quit)").lower()
    
    if not os.path.exists(dom):
        print("No results have been generated")
        return
    else:
        print("Files location: " + dom)

    if res == "y":
        browsingConsole(dom)

def listFiles(path):
    for f in os.listdir(path):
        print(f, end='  ')
    print()

def browsingConsole(dom):
    path = dom
    browse = True
    while browse:
        cmd = input("> ").lower()
        if cmd == "ls":
            listFiles(path)
        elif cmd[:2] == "cd":
            if os.path.exists(path + "/" + cmd[3:]):
                path += "/" + cmd[3:]
            else:
                print("not a valid location")
        elif cmd == "pwd":
            print(path)
        elif cmd == "quit":
            browse = False

    print("ending console session")