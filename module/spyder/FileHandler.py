import sys
import re
import HTTPhandler

class HttpFile:
    respCode = 0

    url, contentType, header, response = "", "", "", ""
    httpHandler = None

    links = []

    def __init__(self, link, httphand):
        self.url = link
        self.httpHandler = httphand
        ioxCount = 0
        getReq = "HEAD " + str(link) + " HTTP/1.1\r\n" + "Host: " + self.httpHandler.dom + "\r\n" + "\r\n"

        self.httpHandler.writeStream(getReq)
        while True:
            try:
                self.header = self.httpHandler.readStream()
                self.respCode = self.getFileCode()
                self.contentType = self.getFileType()

                break
            except IOError:
                self.httpHandler.changeTimeout(5)
                ioxCount += 1

                if ioxCount == 2:
                    break
            except:
                pass
                # apparently first response we get in a set of random bytes, so we dirtly catch it here


    def getFileCode(self):
        # HTTP[^ ]+ (\d+) ([^\n]+)

        return int(re.search("HTTP[^ ]+ (\d+) ([^\n]+)", self.header).group(1))


    def getFileType(self):
        # [Cc]ontent-[Tt]ype: (.+\/.+)\s

        cType = re.search("[Cc]ontent-[Tt]ype: (.+\/.+)\s", self.header).group(1)

        if ';' in cType:
            cType = cType[:cType.find(';')]

        return cType

    def getFileLinks(self):

        #(https?)?[^\w<](\/[\w?\/](?:[^ "'\^]+)?)(\n|"|'|\);)
        regex = r'(https?)?[^\w<](\/[\w?\/](?:[^ "\'\^]+)?)(\n|"|\'|\);)'
        # [] para buscar caracteres específicos, ^ para excluirlos
        # {} indica repeticion de caracteres combinando con secuencias en [] se pueden hacer cosas chascas
        # + al menos uno de el caracter, * cualquier caracter igual (se pueden mandar a tomar por saco si usas {x,y})
        # ? para indicar la opcionalidad del anterior caracter
        #  \s cualquier tipo de espacio: \r return \n linea nueva \t tab ␣ espacio
        #  | or (puerta logica)
        #  \w alfanumerico \d numerico INVERSOS: \W \D
        #  \b para cuando se topa con un caracter no alphabetico (\w+\b)
        linksEX = []
        linksLAN = []

        for match in re.finditer(regex, self.response):
            if not match in linksEX:

                # check if the protocol group is filled
                if not match.group(1) == None:
                    if match.group(2)[1] == '/': #http(s)://something
                        if self.httpHandler.dom in match.group(2)[2:].split('/')[0]: #http(s)://dom/(something)
                            # this link appears to be local, since the domain of the target host is in the url

                            if len(match.group(2)[2:].split('/')) > 1:  #else this means it's just a reference to the host => http(s)://dom/something
                                linksLAN.append('/' + match.group(2)[2:].split('/')[1])
                        else:
                            linksEX.append(match.group(1) + ':' + match.group(2))
                    else:
                        linksEX.append(match.group(1) + ":/" + match.group(2))
                else: #/(/)something
                    # if double slash, add safe protocol and external link
                    if match.group(2)[1] == '/': #//something
                        linksEX.append("https:" + match.group(2))
                    else :#else add host link => #/something
                        linksLAN.append(match.group(2))


        return linksLAN

    # takes up a lot of time to do, so only some of the files will be set up
    def setUp(self):
        print("-------- Receiving file " + self.url + " --------")

        getReq = "GET " + self.url + " HTTP/1.1\r\n" + "Host: " + self.httpHandler.dom + "\r\n" + "\r\n"

        self.httpHandler.writeStream(getReq)

        self.response = self.httpHandler.readRecursive()

        print("-------- File " + self.url + "  received --------")

        print("\n")

        print(">> Getting links in file...")
        self.links = self.getFileLinks()
        print("Done!")

        print("\n")
        print("\n")

    
