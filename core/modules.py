#!/usr/bin/env python

import os
from xmlhandling import *
from design import *
from command import *
import subprocess
import socket
import threading

XML = 'xml'

def TreeModules(path):
	modules = []

	for root, dirs, files in os.walk(path, topdown='true'):
		for name in files:
			file = os.path.join(root, name)
			if file[len(file)-len(XML):] == XML:
				name = GetModuleName(file[:len(file) - 5 - len(XML)])
				modules.append([file[:len(file) - 9], name])

	print(tabulate.tabulate(modules, headers=[design.color.PURPLE + 'Path', 'Name' + design.color.NC]))

def RunModule(main, options, returnInfo):

	command = main
	for arg in GetArgs(options):
		command += " " + arg

	if (returnInfo):
	    callWithReturn(command)
	else:
	    os.system(command)

def GetArgs(options):
	args = []
	for opt in options:
		args.append(opt[1])
	return args

def callWithReturn(command):
	# creation of a socket that recieves information
	serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	# this makes it calm its tits and close the socket as it leaves
	serversocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

	serversocket.bind(("127.0.0.1", 2077))
	serversocket.listen(5)

	# we call the program with the arguments...
	subprocess.Popen(StringSep(command))
	(clientsocket, address) = serversocket.accept()

	# ...and get the return info
	ret = clientsocket.recv(1024).decode()
	print(color.YELLOW + "Return info comming in: " + color.PURPLE + str(ret))

	# finally we close everything
	clientsocket.close()
	serversocket.close()

class Module():
	def __init__(self, path):
		self.info = path + "/info.xml"
		self.path = path
		self.name = GetModuleName(path)
		self.options = GetModuleOptions(path)
		self.main = GetModuleMain(path)
		self.author = GetModuleAuthor(path)
		self.version = GetModuleVersion(path)
		self.language = GetModuleLanguage(path)
		self.interpreter = GetModuleInterpreter(path)
		self.description = GetModuleDescription(path)
		self.support = GetModuleReturnSupport(path)
		self.RunOptions = []

	def ShowOptions(self):
		print(design.color.NC + tabulate.tabulate(self.options, headers=[design.color.PURPLE + 'Option', 'Value', 'Type', 'Description' + design.color.NC], tablefmt="grid"))

	def ShowInfo(self):
		print(design.color.BLUE + 'Name:\n\t' + design.color.YELLOW + self.name + '\v')
		print(design.color.BLUE + 'Author:\n\t' + design.color.YELLOW + self.author + '\v')
		print(design.color.BLUE + 'Version:\n\t' + design.color.YELLOW + self.version + '\v')
		print(design.color.BLUE + 'Description:\n\t' + design.color.YELLOW + self.description + '\v')
		print(design.color.BLUE + 'Language:\n\t' + design.color.YELLOW + self.language + '\v')

	def SetOption(self, option, value):
		for opt in self.options:
			if opt[0].lower() == option.lower():
				opt[1] = value

	def Run(self):
		RunModule(self.interpreter + ' ' + self.path + self.main, self.options, self.support)

class My_Thread(threading.Thread):
	def __init__(self, id, module, func):
			threading.Thread.__init__(self)
			self.id = id
			self.module = module
			self.func = func

	def run(self):
		print("Runing module {0} whit id {1}".format(self.module.name, self.id))
		self.func()
