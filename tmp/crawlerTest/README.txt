to run this, do: 

crawlerTest_2.0.exe [depth] [site domain]

depth - an int value like 3 or 10, 0 for just analizing the url you specified

site domain - [http/https]://(www.)[domain name](/[a file or folder, like robots.txt]) 
for example: https://www.google.com/robots.txt or https://www.youtube.com