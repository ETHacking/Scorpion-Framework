import time
def write_command_to_log(command):
    f = open("log/commands.log", 'a+')

    f.write(command+"\n")
    f.close()
    return command

def write_err_to_log(err):
    f = open("log/errors.log", 'a+')

    f.write("["+time.strftime('%H:%M:%S')+"] "+err+"\n")
    f.close()
