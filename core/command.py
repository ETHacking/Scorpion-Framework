#!/usr/bin/env python

#This code contain all the main commands
import readline
import tabulate
import design

#Help commands
HELP = 'help'
HELP_SHORT1 = 'h'
HELP_SHORT2 = '?'

INFO = 'info'

#Show commands
SHOW = 'show'

#Use commands
USE = 'use'
USE_SHORT = 'u'

#Set commands
SET = 'set'
SET_SHORT = 's'

#Exit commands
QUIT = 'quit'
QUIT_SHORT = 'q'
EXIT = 'exit'
EXIT_SHORT = 'e'

CLEAR = 'clear'
CLEAR_SHORT = 'clr'

OPTIONS = 'options'

RUN = 'run'
RUN_SHORT = 'r'

VERSION = 'version'

class MyCompleter(object):
    def __init__(self, options):
        self.options = sorted(options)

    def complete(self, text, state):
        if state == 0:
            if text:
                self.matches = [s for s in self.options
                                    if s and s.startswith(text)]
            else:
                self.matches = self.options[:]
        try:
            return self.matches[state]
        except IndexError:
            return None

### LOAD BUFFER ###


def LoadBuffer():
    keywords = [HELP, VERSION, QUIT, EXIT, USE, SHOW, CLEAR, CLEAR_SHORT, 'modules', INFO, OPTIONS, ]
    completer = MyCompleter(keywords)
    readline.set_completer(completer.complete)
    readline.parse_and_bind('tab: complete')


def PrintError(command, err):
    error = design.color.ERROR + design.color.YELLOW
    for i in command:
        error += str(i) + " "
    error += ": " + str(err)
    print(error)


def StringSep(string):
    final_string = []
    tmp = ""
    for char in string:
        if char == " ":
            if tmp == "":
                pass
            else:
                final_string.append(tmp)
                tmp = ""
        else:
            tmp += char
    final_string.append(tmp)
    final_string = list(filter(None, final_string))
    tmp = [[]]
    i = 0
    for e in final_string:
        if e == '&&' or e == 'and':
            i += 1
            tmp.append([])
        else:
            tmp[i].append(e)
    final_string = tmp
    return final_string
