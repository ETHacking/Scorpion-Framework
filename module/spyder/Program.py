import sys
import FileHandler
import HTTPhandler
import DirectoryManagement

HTTPhandling = HTTPhandler.HTTPhandling()

def getDomain(url, fileUrl):
    dom = url

    if "://" in url:
        if url[:url.find("://")] == "https":
            HTTPhandling.safe = True
        dom = url[url.find("://") + 3:]

    if len(dom.split('/', 2)) > 1:
        fileUrl[0] += dom.split('/')[1]

    dom = dom.split('/')[0]

    return dom

browsingDepth = int(sys.argv[1])

fu = ["/"]
dom = getDomain(sys.argv[2], fu)
fileUrl = fu[0]

analizedLinks = []

def main():

    print("Connecting.....")

    HTTPhandling.connectSocket(dom)

    print("Connected")

    print("\n")

    index = FileHandler.HttpFile(fileUrl, HTTPhandling)

    if index.respCode != 200:
        print("Unexpected response code: " + index.respCode)
        return 1
    else:
        print("Remote host responded accordingly")

    analized = False
    if input("Start sPyder? (Y/n)").lower() == "y":
        analizeFile(index, 0)
        analized = True

    DirectoryManagement.browse(dom, analized)

    print("bye")

    return 0

def analizeFile(file, depth):
    file.setUp()

    print("")
    print("-------------------------------------------------------------------------")
    print(">> Analizing file " + file.url + " in depth of " + str(depth) + "...")
    print("\\____/-------------------------------------------------------------\\____/")
    print("")

    for link in file.links:

        linkFile = FileHandler.HttpFile(link, HTTPhandling)

        # basic sanitizing of the links

        if basicUrlSanitize(linkFile):
            # here we validate the existance of the file in the host machine

            print(linkFile.url + "  " + str(linkFile.respCode) + " " + linkFile.contentType)

            if linkFile.respCode != 404 and linkFile.respCode != -1:
                # if a link ends up in a slash, we can assume it's a folder reference

                if linkFile.url[len(linkFile.url) - 1] == '/':
                    if '?' in linkFile.url:
                        # this type of url shouldn't contain any arguments
                        print(linkFile.url + " was probably not a file :(")
                    else:
                        DirectoryManagement.createDirs(dom + linkFile.url)
                else:
                    # linkFile.url's value is something like /folder/file.extension or /file.extension at this point
                    path = linkFile.url[:linkFile.url.rfind('/')]
                    fileName = linkFile.url[linkFile.url.rfind('/'):]

                    # here we get rid of all possible arguments given in the url
                    if "?" in fileName:
                        fileName = fileName[:fileName.rfind('?')]

                    if sanitizePathToFile(path, fileName):
                        # if our pressumed file contains a . in it's name, then we can say it's an extension, probably
                        if "." in fileName:
                            DirectoryManagement.createDirsAndFile(dom, path, fileName)
                        else:
                            DirectoryManagement.createDirs(dom + path + fileName)

                if linkFile.respCode == 200 and "text/html" in linkFile.contentType:
                    # this means we have an index file in here
                    if depth < browsingDepth and not linkFile.url in analizedLinks:
                        analizedLinks.append(linkFile.url)
                        analizeFile(linkFile, depth + 1)
        else:
            print(dom + linkFile.url + " was probably not a file :(")

    print(" ____                                                              ____")
    print("/    \\------------------------------------------------------------/    \\")
    print(">> Analisis of file " + file.url + " in depth of " + str(depth) + " concluded")
    print("-------------------------------------------------------------------------")
    print()

def sanitizePathToFile(path, fileName):

    # this wouldnt be possible
    if "?" in path:
        print(dom + path + fileName + " was probably not a file :(")
    return not "?" in path

def basicUrlSanitize(linkFile):
    if "\\" in linkFile.url:
        linkFile.url = linkFile.url.replace("\\", "")

    badChars = ['"', '<', '>', '|']

    contains = False

    for char in badChars:
        if char in linkFile.url:
            contains = True

    return not contains

main()