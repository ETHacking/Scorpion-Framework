from xml.dom import minidom
import design

def GetModuleName(path):
	xmldoc = minidom.parse(path + '/info.xml')
	name = xmldoc.getElementsByTagName('name')[0].firstChild.data
	return name


def GetModuleDescription(path):
	xmldoc = minidom.parse(path + '/info.xml')
	description = xmldoc.getElementsByTagName('description')[0].firstChild.data
	return description


def GetModuleAuthor(path):
	xmldoc = minidom.parse(path + '/info.xml')
	author = xmldoc.getElementsByTagName('author')[0].firstChild.data
	return author


def GetModuleVersion(path):
	xmldoc = minidom.parse(path + '/info.xml')
	v = xmldoc.getElementsByTagName('version')[0].firstChild.data
	return v


def GetModuleMain(path):
	xmldoc = minidom.parse(path + '/info.xml')
	main = "/"+xmldoc.getElementsByTagName('main')[0].firstChild.data
	return main

def GetModuleLanguage(path):
	xmldoc = minidom.parse(path + '/info.xml')
	language = xmldoc.getElementsByTagName('language')[0].firstChild.data
	return language

def GetModuleReturnSupport(path):
	
	xmldoc = minidom.parse(path + '/info.xml')
	if (xmldoc.getElementsByTagName('return').length > 0):
		return True
	else:
		return False

def GetModuleInterpreter(path):

	interpreter = ''
	if GetModuleLanguage(path) == 'python':
		interpreter = 'python';
	elif GetModuleLanguage(path) == 'C#':
		interpreter = 'mono';

	return interpreter

def GetModuleOptions(path):
	xmldoc = minidom.parse(path + '/info.xml')
	options = xmldoc.getElementsByTagName('opt')
	final_options = []
	for opt in options:
		name = opt.getElementsByTagName('name')[0].firstChild.data
		try:
			value = opt.getElementsByTagName('default')[0].firstChild.data
		except:
			value = ""
		description = opt.getElementsByTagName('description')[0].firstChild.data
		type = opt.getElementsByTagName('type')[0].firstChild.data
		final_options.append([name, value, type, description])
	return final_options
