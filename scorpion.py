#!/usr/bin/env python3
#python3 version of Scorpion, adapted by Marcos Freire :D

################################################################################
################################################################################
##        _________                          __                               ##
##       /   _____/ ____  _________________ |__| ____   ____  (py3)           ##
##       \_____  \_/ ___\/  _ \_  __ \____ \|  |/  _ \ /    \                 ##
##       /        \  \__(  <_> )  | \/  |_> >  (  <_> )   |  \                ##
##      /_______  /\___  >____/|__|  |   __/|__|\____/|___|  /                ##
##              \/     \/            |__|                  \/                 ##
##                 Developed By Flavian Dei                                   ##
##                                    Supported By ETHacking                  ##
##                                                                            ##
################################################################################
################################################################################
##SCORPION FRAMEWORK ( (c) 2018 EThacking)									  ##
################################################################################
################################################################################
##This is the first version of the code of Scorpion. You are not allowed      ##
##to share the code, in any part and form.                                    ##
##You are not allowed to share any screen or information about scorpion       ##
################################################################################
##If you found this code in internet, please report                           ##
##this to flaviandeipimen@gmail.com.                                          ##
################################################################################
##If you are a tester, please report any bug or idea to                       ##
##flaviandeipimen@gmail.com or contact me in private chat.  	              ##
################################################################################
################################################################################

#Load the libraries
try:
	import sys

	sys.path.append('core/')
	print('[\033[0;34mi\033[0m] ' + 'Loading system libraries...' + '\033[0m')
	import os
	print ('[\033[0;34mi\033[0m] ' + 'loading scorpion libraries...' + '\033[0m')
	from core.design import *
	import core.design
	from core.command import *
	from core.help import *
	from core.modules import *
#	from args import *
	from core.xmlhandling import *
	print(design.color.KERNELSUCCESS + 'libraries loaded successful' + design.color.NC)
except:
	print('[\033[0;31mfailed\033[0m] ' + "error occurred while loading libraries" + '\033[0m')

# def init():
# 	Debug = DebugMode(sys.argv)
#


def main():

	version = 0.3
	module = 'SCORPION'
	CmdCicle = 0
	command = []
	print('ciao')
	#Check the folder structure
	print(design.color.NOTIFICATION + "checking folder structure")
	if not os.path.isdir("data/") or not os.path.isdir("module/"):
		print(design.color.KERNELERROR + "error occurred while checking folder structure")
		print(design.color.NOTIFICATION + "exiting the Scorpion Framework")
		exit()
	else:
		print(design.color.KERNELSUCCESS + "folder structure checked successful")

	#Show banner
	os.system('clear')
	banner()

	print(color.KERNELWARNING + color.RED + "THIS IS NOT A PUBBLIC VERSION. PLEASE DON'T SHARE THE CODE.")
	try:
		while 1:
			if len(command) <= 1 or CmdCicle+1 >= len(command):
				command = StringSep(input(str(design.main_cli(module))))#Take the CLI input and transform it to a list.
				CmdCicle = 0
			else:
				CmdCicle += 1
			print(str(command))

			if len(command[CmdCicle]) < 1:
				continue
			#Check for the help command
			if (command[CmdCicle][0] == HELP or command[CmdCicle][CmdCicle] == HELP_SHORT1 or command[CmdCicle][0] == HELP_SHORT2):
				help()

			#Check for the quit commands
			elif command[CmdCicle][0] == QUIT or command[CmdCicle][0] == QUIT_SHORT and len(command) == 1:
				os.system('clear')
				print('GOODBYE! :D')  #NOTE: too short :P
				exit()

			#Check for the show commands
			elif command[CmdCicle][0] == SHOW:
				try:
					if command[CmdCicle][1] == 'modules':
						TreeModules('module')	#Show all the modules
					elif command[CmdCicle][1] == 'options':
						PrintError(command[CmdCicle], "YOU AREN'T IN A MODULE, PLEASE USE THE COMMAND 'use' FOR START TO USE A MODULE.")
					else:
						PrintError(command[CmdCicle], "nothing to show")
				except:
					PrintError(command[CmdCicle], 'nothing to show')

			elif command[CmdCicle][0] == CLEAR or command[CmdCicle][0] == CLEAR_SHORT and len(command) == 1:
				os.system('clear')

			elif command[CmdCicle][0] == INFO:
				try:
					xmldoc = command[CmdCicle][1]
					if os.path.isfile(xmldoc + '/info.xml'): #check if the module exist
						print(design.color.BLUE + 'Name:\n\t' + design.color.YELLOW + GetModuleName(xmldoc) + '\v')
						print(design.color.BLUE + 'Author:\n\t' + design.color.YELLOW + GetModuleAuthor(xmldoc) + '\v')
						print(design.color.BLUE + 'Version:\n\t' + design.color.YELLOW + GetModuleVersion(xmldoc) + '\v')
						print(design.color.BLUE + 'Description:\n\t' + design.color.YELLOW + GetModuleDescription(xmldoc) + '\v')
					else:
						print(design.color.ERROR + design.color.YELLOW + 'Use a valid module name.')
				except:
					print(design.color.ERROR + design.color.YELLOW + 'Use a valid module name.')

##############################################################################################
#							USE COMMAND LINE                                                 #
##############################################################################################
			elif (command[CmdCicle][0] == USE or command[CmdCicle][0] == 'u'):
				if os.path.isfile(command[CmdCicle][1] + '/info.xml'):#check if the module exist
					module = Module(command[CmdCicle][1])
					while 1:  #The loop for the module prompt
						if len(command) <= 1 or CmdCicle+1 >= len(command):
							command = StringSep(input(str(design.main_cli(module.name))))#Take the CLI input and transform it to a list.
							CmdCicle = 0
						else:
							CmdCicle += 1
						#Check for the exit commands
						if command[CmdCicle][0] == EXIT or command[CmdCicle][0] == EXIT_SHORT and len(command) == 1:
							print(color.NOTIFICATION + 'Exit from the module...')
							module = 'SCORPION'
							break

						#Check for the use command
						elif command[CmdCicle][0] == USE or command[CmdCicle][0] == USE_SHORT:
							#check if the module exist
							if os.path.isfile(command[CmdCicle][1] + '/info.xml'):
								#Change the module
								module = Module(command[CmdCicle][1])
							else:
								print(design.color.ERROR + design.color.YELLOW + 'Use a valid module name.')
						#Check for the help command
						elif command[CmdCicle][0] == HELP or command[CmdCicle][0] == HELP_SHORT1 or command[CmdCicle][0] == HELP_SHORT2 and len(command) == 1:
							help()

						#check for the show commands
						elif command[CmdCicle][0] == SHOW:
							try:
								#Check if the second argument is modules
								if command[CmdCicle][1] == 'modules':
									#Show all the modules
									TreeModules('module')
								#Check if the second argument is options
								elif command[CmdCicle][1] == OPTIONS:
									#Show the options
									module.ShowOptions()
								else:
									PrintError(command[CmdCicle], "nothing to show.")
							except:
								PrintError(command[CmdCicle], "nothing to show.")
						#Check for the clear commands
						elif command[CmdCicle][0] == CLEAR or command[CmdCicle][0] == CLEAR_SHORT and len(command) == 1:
							os.system('clear')

						#Check for the quit commands
						elif command[CmdCicle][0] == QUIT or command[CmdCicle][0] == QUIT_SHORT and len(command) == 1:
							os.system('clear')
							print(design.color.OK + 'See you later')
							exit()

						elif command[CmdCicle][0] == INFO:
							#Check if the user want to see te info about the current module
							try:
								xmldoc = Module(command[CmdCicle][1])
							except IndexError:
								xmldoc = module
							if os.path.isfile(xmldoc.info): #check if the module exist
								xmldoc.ShowInfo()
							else:
								PrintError(command[CmdCicle], "Use a valid module name.")


						elif command[CmdCicle][0] == VERSION and len(command) == 1:
							print(design.color.BLUE + 'Version: ' + design.color.GREEN + str(module.version))

						elif command[CmdCicle][0] == SET or command[CmdCicle][0] == SET_SHORT:
							if len(command[CmdCicle]) > 1:
								option = command[CmdCicle][1]
								value = ''
								i = 2
								while i < len(command[CmdCicle]):
									value += ' ' + command[CmdCicle][i]
									i += 1

								module.SetOption(option, value)
							else:
								PrintError(command[CmdCicle], "INVALID OPTION.")

						elif command[CmdCicle][0] == RUN:
							if len(command[CmdCicle]) > 1:
								if command[CmdCicle][1] == '-j': #Job opt
										Thread = My_Thread(1, module, module.Run)
										Thread.start()
							else:
								module.Run()

						#If the command dont exist, print an error message
						else:
							PrintError(command[CmdCicle], "COMMAND NOT FOUND. USE \"HELP\" FOR DISPLAY THE COMMAND LIST")

				#If the module don't exist, print an error message
				else:
					PrintError(command[CmdCicle], "Use a valid module name.")
					module = 'SCORPION'

##############################################################################################
#							USE COMMAND LINE                                                 #
##############################################################################################

			elif command[CmdCicle][0] == VERSION and len(command) == 1:
				print(design.color.BLUE + 'Version: ' + design.color.GREEN + str(version))

			else:
				PrintError(command[CmdCicle], 'COMMAND NOT FOUND. USE "HELP" FOR DISPLAY THE COMMAND LIST')


	except KeyboardInterrupt:
		os.system('clear')
		print(design.color.OK + 'See you later')
		exit()

#	except:
#		os.system('clear')
#		print(design.color.ERROR + design.color.YELLOW + 'Unexpected error occured.')
#		exit()


if __name__ == '__main__':
#	init()
	LoadBuffer()
	main()
