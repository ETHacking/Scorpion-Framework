#!/usr/bin/env python

from threading import Thread
import socket
import sys

#if len(sys.argv) < 4:
#	print('[-] set all option pleas.')
#	exit()

class Scanner(Thread):
	def __init__(self, host, from_port, to_port):
		Thread.__init__(self)
		self.host = str(host)
		self.from_port = int(from_port)
		self.to_port = int(to_port)
	def run(self):
		s = socket.socket()
		p = self.from_port
		while p <= self.to_port:
			result = s.connect_ex((self.host, p))
			#print('working on port > '+(str(port)))
			if result == 0:
				print(str(p) + " OPEN")
			else:
				print(str(p)+" CLOSE")
				s.close()

test = Scanner('216.58.201.164', 1, 100)

test.start()

test.join()
