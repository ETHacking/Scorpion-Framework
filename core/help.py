#/usr/bin/env python
import design
import tabulate

def help():
	help = []
	help.append(['help OR h OR ?', '', 'Show all the commands'])
	help.append(['show modules', '', 'Show all the modules'])
	help.append(['use OR u', '[module]', 'Select a module'])
	help.append(['set OR s', '[option] [parameter]', 'Set module options'])
	help.append(['exit OR e', '', 'Exit from a module'])
	help.append(['version', '', 'Show the currenct version'])
	help.append(['clear OR clr', '', 'Clear the screen'])
	help.append(['quit OR q', '', 'Quit from the program'])
	help.append(['info', '[module]', 'Show information about the module'])
	help.append(['run', 'Run the module\n'])
	print(tabulate.tabulate(help, tablefmt="simple"))
